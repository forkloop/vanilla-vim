" Not compatible with Vi
set nocompatible

call pathogen#infect()
call pathogen#helptags()

" Reload vimrc when edited
autocmd! bufwritepost .vimrc source ~/.vimrc

filetype on
filetype plugin on
filetype indent on
syntax on
runtime! macros/matchit.vim

set nowrap
set linebreak
"set textwidth=120
set novb
set title
set guicursor=a:blinkon0                        " stop cursor blinking
set encoding=utf-8
set history=1000                                 " keep 500 lines of command line history
set ruler                                       " show the cursor position all the time
set showcmd                                          " showcmd, make vim learning easily
set autoread                                    " set to auto read when a file is changed outside
set hlsearch                                    " highlight search
set magic
set noerrorbells                                " disable sound when error
set confirm                                     " confirm before quit
set autoindent
set showmatch                                   " show matching brackets
set expandtab
set tabstop=4
set scrolloff=3
set matchtime=6
set softtabstop=4
set shiftwidth=4
set number
set backup
set backupskip=/tmp/*,/private/tmp/*
set backupdir=~/.vim/backup
set directory=~/.vim/tmp
set laststatus=2
set cmdheight=1
set clipboard=unnamed                           " yank and paste with system clipboard
set backspace=indent,eol,start
" Close omnicomplete preview window
set completeopt-=preview
" Display tab and end whitespace
set list
set listchars=tab:▸\ ,eol:¬

" Powerline
"set noshowmode
"set laststatus=2
"set cmdheight=1
"set rtp+=/Users/liuxl/Library/Python/2.7/lib/python/site-packages/powerline/bindings/vim

if has("gui_running")
    set guioptions-=T
    set guifont=Consolas:h14
    colorscheme molokai
else
    colorscheme molokai
endif
hi Comment guifg=#95a5a6 gui=italic

" Reformatting
map <silent> <F5> mmgg=G'm
imap <silent> <F5> <ESC> mmgg=G'm

"====
" Abbreviation
iabbrev utf8coding -*- coding: utf-8 -*-

"=========================================
"=== Mappings, never use *map ============
let mapleader = ","
nnoremap <leader>w <C-w>v<C-w>l

" Delete current line in insert mode
"inoremap <C-d> <ESC>ddi

" Insert to next line in insert mode
inoremap <S-CR> <ESC>o
inoremap <S-C-CR> <ESC>O

inoremap <C-space> <C-x><C-o>

" Easy jump between panes

nnoremap <C-j> <C-w>j
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap <C-k> <C-w>k

" plugins

nnoremap <F3> :TagbarToggle<CR> 
let g:tagbar_left = 1
nnoremap <F4> :NERDTreeToggle<CR>
let g:NERDTreeWinPos="right"

nnoremap <F6> :CommandTFlush<CR>

" ctags
set tags+=./tags;

let g:ctags_statusline = 1
"===
" Perl


"===
" Delimimate


"===
" Ruby
autocmd BufRead,BufNewFile *.rb setlocal softtabstop=2
autocmd BufRead,BufNewFile *.rb setlocal shiftwidth=2
autocmd BufRead,BufNewFile *.rb set tags+=~/.vim/tags/rails.ctags
"autocmd BufRead,BufNewFile *.rb set 
